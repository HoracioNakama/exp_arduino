#include <DHT.h>
#include <MySQL_Connection.h>
#include <MySQL_Cursor.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>

#define DHTPIN D2
#define DHTTYPE DHT22
//#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);

#define sensorPin1 0

//#define sensorPin2 D2
//#define typeDHT DHT11
//DHT dht(sensorPin1, typeDHT);

char ssid[] = "TP-LINK_8A1C";                 // Network Name
char pass[] = "cuidatecuidanoscuidemos";                 // Network Password
//char ssid[] = "wFCA";                 // Network Name
//char pass[] = "fcA2017AcE4";                 // Network Password
byte mac[6];

WiFiServer server(80);
IPAddress ip(192, 168, 0, 100);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

WiFiClient client;
MySQL_Connection conn((Client *)&client);

//char INSERT_SQL[] = "INSERT INTO prueba.probando(`nombre`, `dato`, `obs`) VALUES ('rasp',4,'prueba')";
char INSERT_SQL[] = "INSERT INTO prueba.probando2(`nombre`, `temp`, `hum`, `obs`) VALUES ('borrar', %.1f, %.1f, 'borrar')";
char query[128];

// IPAddress server_addr(10, 5, 0, 29);          // MySQL server IP
IPAddress server_addr(192, 168, 0, 200);          // MySQL server IP
char user[] = "arduino";           // MySQL user
char password[] = "arduinO314!";       // MySQL password

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
  dht.begin();

  pinMode(sensorPin1, INPUT);
  //pinMode(sensorPin2, INPUT);

  Serial.println("Initialising connection");
  Serial.print(("Setting static ip to : "));
  Serial.println(ip);

  Serial.println("");
  Serial.println("");
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.config(ip, gateway, subnet); 
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(200);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi Connected");

  WiFi.macAddress(mac);
  Serial.print("MAC: ");
  Serial.print(mac[5],HEX);
  Serial.print(":");
  Serial.print(mac[4],HEX);
  Serial.print(":");
  Serial.print(mac[3],HEX);
  Serial.print(":");
  Serial.print(mac[2],HEX);
  Serial.print(":");
  Serial.print(mac[1],HEX);
  Serial.print(":");
  Serial.println(mac[0],HEX);
  Serial.println("");
  Serial.print("Assigned IP: ");
  Serial.print(WiFi.localIP());
  Serial.println("");
  Serial.println("Connecting to database");

  while (conn.connect(server_addr, 3306, user, password) != true) {
    delay(200);
    Serial.print ( "." );
  }

  Serial.println("");
  Serial.println("Connected to SQL Server!");  
}

void loop() {
  //int soil_hum = 1024 - analogRead(sensorPin1);
  //float t = dht.readTemperature();
  //Serial.println(t);
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  Serial.println(t);
  Serial.println(h);
  
  //sprintf(query, INSERT_SQL, soil_hum);
  //sprintf(query, INSERT_SQL, soil_hum, t);
  sprintf(query, INSERT_SQL, t, h);

  Serial.println("Recording data.");
  Serial.println(query);
  
  MySQL_Cursor *cur_mem = new MySQL_Cursor(&conn);
  
  cur_mem->execute(query);
  
  delete cur_mem;
  
  //digitalWrite(LED_BUILTIN, LOW); 
  //delay(1000);
  //digitalWrite(LED_BUILTIN, HIGH);

  //delay(600000); //10 min
  //delay(597000); //10 sec
  delay(3000);
  //delay(8000); //10 sec
}
