#define sensor A0
#include <Servo.h>

Servo myservo;
 
void setup(){
  pinMode(sensor, INPUT);
  Serial.begin(9600);
  myservo.attach(2);
}
 
void loop(){
  //Se hace la lectura analoga del pin A0 (sensor) y se pasa por la funcion
  //map() para ajustar los valores leidos a los porcentajes que queremos utilizar   
  int valorHumedad = map(analogRead(sensor), 0, 1023, 100, 0);
  delay(150);
  int ang = map(valorHumedad, 0, 100, 0, 180);
  delay(2000);
  myservo.write(ang);
  delay(100);
  Serial.print("Angulo: ");
  Serial.print(ang);
  Serial.println("°");
  delay(100);
  Serial.print("Humedad: ");
  Serial.print(valorHumedad);
  Serial.println("%\n");
  delay(1000);
  myservo.write(0);
  delay(4000);
}
