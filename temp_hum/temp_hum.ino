//Para NODEMCU
//#include "DHTesp.h"
#include <DHT.h>

// DHTesp dht;
#define DHTPIN D2     // what digital pin the DHT22 is conected to
#define DHTTYPE DHT22   // there are multiple kinds of DHT sensors

DHT dht(DHTPIN, DHTTYPE);
 
void setup() {
    Serial.begin(9600);
    delay(10);
    dht.begin();
//    dht.setup(D2, DHTesp::DHT22);
}
 
void loop() {
    float h = dht.readHumidity();
    float t = dht.readTemperature();
 
    Serial.print("{\"humidity\": ");
    Serial.print(h);
    Serial.print(", \"temp\": ");
    Serial.print(t);
    Serial.print("}\n");
 
    delay(2000);
}
