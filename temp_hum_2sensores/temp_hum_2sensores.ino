#include <DHT.h>
#include <MySQL_Connection.h>
#include <MySQL_Cursor.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>

#define DHTPIN22 D2 
#define DHTTYPE22 DHT22  // blanco

#define DHTPIN11 D3
#define DHTTYPE11 DHT11  // celeste

// Código actualizado el 07/08/2020

DHT dht22(DHTPIN22, DHTTYPE22);
DHT dht11(DHTPIN11, DHTTYPE11);

#define sensorPin1 0

//#define sensorPin2 D2
//#define typeDHT DHT11
//DHT dht(sensorPin1, typeDHT);

char ssid[] = "TP-LINK_8A1C";                 // Network Name
char pass[] = "cuidatecuidanoscuidemos";                 // Network Password
//char ssid[] = "wFCA";                 // Network Name
//char pass[] = "fcA2017AcE4";                 // Network Password
byte mac[6];

WiFiServer server(80);
IPAddress ip(192, 168, 0, 100);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

WiFiClient client;
MySQL_Connection conn((Client *)&client);

//char INSERT_SQL[] = "INSERT INTO prueba.probando(`nombre`, `dato`, `obs`) VALUES ('rasp',4,'prueba')";
char INSERT_SQL22[] = "INSERT INTO prueba.arduino_ext(`temp`, `hum`, `obs`) VALUES (%.1f, %.1f, 'dht22_ext')";
char INSERT_SQL11[] = "INSERT INTO prueba.arduino_int(`temp`, `hum`, `obs`) VALUES (%.1f, %.1f, 'dht11_ext')";
char query[128];

// IPAddress server_addr(10, 5, 0, 29);          // MySQL server IP
IPAddress server_addr(192, 168, 0, 200);          // MySQL server IP
char user[] = "arduino";           // MySQL user
char password[] = "arduinO314!";       // MySQL password

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.begin(9600);
  dht22.begin();
  dht11.begin();

  pinMode(sensorPin1, INPUT);
  //pinMode(sensorPin2, INPUT);

  Serial.println("Initialising connection");
  Serial.print(("Setting static ip to : "));
  Serial.println(ip);

  Serial.println("");
  Serial.println("");
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.config(ip, gateway, subnet); 
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(200);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi Connected");

  WiFi.macAddress(mac);
  Serial.print("MAC: ");
  Serial.print(mac[5],HEX);
  Serial.print(":");
  Serial.print(mac[4],HEX);
  Serial.print(":");
  Serial.print(mac[3],HEX);
  Serial.print(":");
  Serial.print(mac[2],HEX);
  Serial.print(":");
  Serial.print(mac[1],HEX);
  Serial.print(":");
  Serial.println(mac[0],HEX);
  Serial.println("");
  Serial.print("Assigned IP: ");
  Serial.print(WiFi.localIP());
  Serial.println("");
  Serial.println("Connecting to database");

  while (conn.connect(server_addr, 3306, user, password) != true) {
    delay(200);
    Serial.print ( "." );
  }

  Serial.println("");
  Serial.println("Connected to SQL Server!");  
}

void loop() {
  float h22 = dht22.readHumidity();
  float t22 = dht22.readTemperature();
  float h11 = dht11.readHumidity();
  float t11 = dht11.readTemperature();
  //Serial.println(t22);
  //delay(1000);
  //Serial.println(h22);
  //delay(1000);
  //Serial.println(t11);
  //delay(1000);
  //Serial.println(h11);
  MySQL_Cursor *cur_mem = new MySQL_Cursor(&conn);
  
  sprintf(query, INSERT_SQL22, t22, h22);
  Serial.println("Recording data 22.");
  digitalWrite(LED_BUILTIN, LOW);
  delay(2000);
  digitalWrite(LED_BUILTIN, HIGH);
  //Serial.println(query);
  cur_mem->execute(query);
  
  sprintf(query, INSERT_SQL11, t11, h11);
  Serial.println("Recording data 11.");
  digitalWrite(LED_BUILTIN, LOW);
  delay(2000);
  digitalWrite(LED_BUILTIN, HIGH);
  //Serial.println(query);
  cur_mem->execute(query);
  delete cur_mem;
  
  delay(900000);
}
