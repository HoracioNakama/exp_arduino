# ARCHIVOS ARDUINO 

NOTA: los archivos que inician con "probando" son pruebas

## HUM_SUELO

Código para el sensor de Humedad de Suelo

Muestra los datos en el monitor serial

Obtenido por Jairo Araoz

## TEMP_HUM

Código para el sensor de Temperatura y Humedad DHT22

Muestra los datos en el monitor serial

## TEMP_HUM_CBBA

Código para el sensor de Temperatura y Humedad DHT22

Usado en la cátedra para cargar los datos a una base de datos Mysql

## SERVO

Código para el sensor de Servo

NOTA: en el WEMOS D1 debe usarse el puerto D4 (en el código figura el puerto 2 GI02)
